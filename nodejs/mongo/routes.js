// routes.js
// Подключаем модули с реализацией роутов
const addNewPost = require('./app/posts/add') 
const getPost = require('./app/posts/getPost')
const updPost = require('./app/posts/upd')
const addUser = require('./app/user/addUser')
const showUserInfo = require('./app/user/getUser')
const deleteUser = require('./app/user/deleteUser')

module.exports = function(app, db) {
    addNewPost(app, db);
    getPost(app, db);
    updPost(app, db);
    addUser(app, db);
    showUserInfo(app, db);
    deleteUser(app, db);
}
