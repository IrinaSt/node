const ObjectId = require('mongodb').ObjectId

module.exports = function(app, db) {
    app.get('/upd', async (req, res) => {
        let query = { _id: ObjectId(req.body.id) }; // условия которым соответствует документ который хотим обновить
        let infoForUpd = {body: req.body.body} // что именно будем обновлять
        let result;
        try {
            result = await db.collection('posts')
                .updateOne(query, {$set: infoForUpd}, {upsert: true}); // обновление информации в базе данных
        } catch (err) {
            console.log(err);
        }
        console.log(result)
        res.end('OK!');
    })
}