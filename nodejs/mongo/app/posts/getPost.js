const ObjectId = require('mongodb').ObjectId

module.exports = function(app, db) {
    app.get('/getPost', async (req, res) => {
        let query = {_id: ObjectId(req.body.id)}; // условие по которому будет осуществляться поиск
        let result;
        try{
            result = await db.collection('posts').findOne(query) 	
        } catch (err) { 
            console.log(err)
        }
        console.log(result)
        res.end(JSON.stringify(result))
    })
}