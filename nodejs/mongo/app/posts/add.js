module.exports = function(app, db) {
    app.post('/add', async (req, res) => {
        let post = { // объект который мы будем добавлять
          title: req.body.title,
          body: req.body.body
        }
        let result;
        try{
            // указываем в какую коллекцию мы хотим добавить объект и сам объект
            result = await db.collection('posts').insertOne(post) 	
        } catch (err) { 
            // was some error when we try add new user
            console.log(err)
        }
        res.end(JSON.stringify(result))
    })
}