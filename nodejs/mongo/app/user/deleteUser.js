const ObjectId = require('mongodb').ObjectId

module.exports = (app, db) => {
    app.get('/deleteUser', async (req, res) => {
        let query = { _id: ObjectId(req.body.id)} // условие запроса
        let result = null
        try {
            result = await db.collection('user').deleteOne(query) //удаление одного элемента соответствующего запросу
        }
        catch(err){
            console.log(err)
        }
        res.end('Response ended user deleted goodbye')
    })
}