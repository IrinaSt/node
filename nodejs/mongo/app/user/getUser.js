const ObjectId = require('mongodb').ObjectId

module.exports = (app, db) => {
    app.get('/getUser', async (req, res) => {
        let query = {_id: ObjectId(req.body.id)} // усдлвия поиска
        let result = null;
        try {
            result = await db.collection('user').findOne(query) //поиск одного элемента соответствующего запросу
        }
        catch(err){
            console.log(err)
        }
        res.end(JSON.stringify(result))
    })
}